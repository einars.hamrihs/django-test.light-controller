      window.addEventListener("load", function(){

        /* Connection configuration to our WAMP router */
        var connection = new autobahn.Connection({
           url: 'ws://172.19.0.2:8080/ws',
           realm: 'realm1'
        });

        /* When the connection is opened, execute this code */
        connection.onopen = function(session) {
            console.log("connected");
          session.subscribe("light-changed", function(args){
            location.reload();
          });

        };
        connection.onerror = function(session) {
            console.log("error: ",session);
        }
        // Open the WAMP connection with the router.
        connection.open();

      });
