from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from .light import Light, RequestProtocol
from .models import Lamp, set_up_db

light = Light()


def light_view(request: RequestProtocol):
	light_val = light.get_light()
	return render(request, 'view.html', {"light": light_val})

@csrf_exempt
def light_controller(request: RequestProtocol):
	lamp = Lamp.objects.get(lamp_name="main")
	room = lamp.room
	if request.method == "POST":
		room = light.update_room(request)

	return render(request, 'controller.html',
		{"daylight": room.daylight, "sun": room.sun, "curtain": room.curtain})

set_up_db()