from typing import Protocol

import requests

from .env_settings import Settings
from .models import Lamp, Room

settings = Settings()

class RequestProtocol(Protocol):
	def method(self) -> str:
		...

	def POST(self) -> str:
		...


class Light:
    @staticmethod
    def get_light() -> int:
        lamp = Lamp.objects.get(lamp_name="main")
        light = lamp.get_brightness() * 5
        return light

    @staticmethod
    def update_room(request: RequestProtocol, room: Room) -> Room:
        room.daylight = request.POST.get("daylight", 5)
        room.sun = request.POST.get("sun", 5)
        room.curtain = request.POST.get("curtain", 5)
        room.save()
        requests.post(f"{settings.notify_url}/notify",
                      json={
                          "topic": "light-changed",
                          "args": []
                      })

        return room
