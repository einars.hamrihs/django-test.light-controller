from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
import uuid


class Room(models.Model):
    curtain = models.IntegerField(
        default=5, validators=[MinValueValidator(1), MaxValueValidator(5)]
    )
    sun = models.IntegerField(
        default=5, validators=[MinValueValidator(1), MaxValueValidator(5)]
    )
    daylight = models.IntegerField(
        default=5, validators=[MinValueValidator(1), MaxValueValidator(5)]
    )


class Lamp(models.Model):
    lamp_name = models.CharField(unique=True, max_length=100, default=uuid.uuid1)
    room = models.OneToOneField(Room, on_delete=models.CASCADE)

    def get_brightness(self) -> int:
        return (self.room.sun + self.room.daylight) / 5 * self.room.curtain


def set_up_db():
    Lamp.objects.all().delete()
    Room.objects.all().delete()

    r = Room()
    r.save()
    l = Lamp(lamp_name="main", room=r)
    l.save()
