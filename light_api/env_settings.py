from dotenv import load_dotenv
from pydantic import BaseSettings, Field

load_dotenv(verbose=True)

class Settings(BaseSettings):
    # default values to None for unit tests
    component_name: str = "ligh-controller"
    notify_url: str = Field(None, env="NOTIFY_URL")
