from django.urls import path
from . import views

urlpatterns = [
	path('', views.light_view, name='light_view'),
	path('controller/', views.light_controller, name='light_controller'),
]
