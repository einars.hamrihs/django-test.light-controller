from django.test import TestCase
from light_api.models import Room, Lamp
from light_api.light import Light
from unittest import mock

# Create your tests here.
class LightTestCase(TestCase):
    def setUp(self) -> None:
        self.settings_mocker = mock.patch(
            "light_api.light.settings",
            mock.MagicMock(notify_url="http://127.0.0.1"),
        )
        self.settings_mocker.start()
        r = Room()
        Lamp(lamp_name="main", room=r)
        self.light = Light()

    def tearDown(self) -> None:
        self.settings_mocker.stop()

    def test_get_light(self):
        self.assertEqual(self.light.get_light(), 50)

    def test_update_room(self):
        post_mock = mock.MagicMock()
        requests_mocker = mock.patch(
            "light_api.light.requests.post"
            ,post_mock
        )
        requests_mocker.start()
        new_room = {"daylight": 1, "sun": 2, "curtain": 3}
        mock_new_room = mock.MagicMock(POST=new_room)
        old_room = Room(daylight=5, sun=5, curtain=5)
        updated_room = self.light.update_room(mock_new_room, old_room)
        self.assertEqual(updated_room.daylight, 1)
        self.assertEqual(updated_room.sun, 2)
        self.assertEqual(updated_room.curtain, 3)
        post_mock.assert_called()
