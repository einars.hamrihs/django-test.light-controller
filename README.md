# django.test-light-controller
* Lamp adjust itself based on the natural light in the room.
* Control the natural light in the room from web interface

Works together with crossbar. Api sends notification to crossbar after every light update. Light view listens to notifications and updates the lamp view.

## How to run locally

### Pre-requisites

> python3.11 -m venv $PROJECT_DIR/venv
> source $_PROJECT_DIR/venv/bin/activate
> pip install -r requirements.txt
> python manage.py runserver

## flake8, black pre-commit

before submitting changes to repository, pre-commit git hook automatically checks the code for errors and does some
formatting automatically. This is done by flake8 and black.

configuration files:
- .pre-commit-config.yaml
- .flake8
- pyproject.toml

> **pre-commit install**
> pre-commit run --file=light_api/light.py --file=light_api/views.py --file=light_api/models.py

if you need to ignore these checks run:
> git commit -m 'commit message' --no-verify

Test:
> python manage.py test
